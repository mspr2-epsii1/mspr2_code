using System;
namespace mspr2_code{
    public class CompteBancaire{
        public double sold = 0.00;
        public double debit = 0.00;
        public double credit = 0.00;

        public void AddMoney(double lcAmount){
            sold += lcAmount;
        }
        public void DropMoney(double lcAmount){
            sold -= lcAmount;
        }
        public bool tryOne(){
            AddMoney(100);
            DropMoney(99.5);
            AddMoney(1);

            if (sold < 0.0)
            {
                return true;
            }
            return false;
        }
    }
}