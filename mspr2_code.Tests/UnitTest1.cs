using System;
using Xunit;
using mspr2_code;

namespace mspr2_code.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void CheckSold()
        {
            var lcAccount = new CompteBancaire();
            Assert.False(lcAccount.tryOne(),"[UNIT ERREUR] Le solde " + lcAccount.sold.ToString() + " ne peux pas être négatif");
        }
    }
}
