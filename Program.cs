﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mspr2_code;

namespace mspr2_code
{
    class Program
    {
        static void Main()
        {
            var compte = new CompteBancaire();
            int choix = 0;
            Console.OutputEncoding = Encoding.UTF8;

            Console.WriteLine("Bienvenue sur votre espace banquaire.\n");

            while (choix != 4)
            {
                Console.WriteLine("Quelle action voulez-vous entreprendre ?\n");
                Console.WriteLine("1 - Consulter votre solde\n");
                Console.WriteLine("2 - Débiter votre compte\n");
                Console.WriteLine("3 - Créditer votre compte\n");
                Console.WriteLine("4 - Quitter l'application\n");
                choix = Convert.ToInt32(Console.ReadLine());
                if (choix == 1)
                {
                    Console.WriteLine("Votre compte possède : " + compte.sold.ToString() + " €\n");
                }
                else if (choix == 2)
                {
                    bool keepTrying = true;
                    int yesno;
                    bool notInErrorFormat = true;
                    while (keepTrying)
                    {
                        Console.WriteLine("Combien d'argent voulez-vous débiter sur votre compte, qui possède " + compte.sold.ToString() + " € ?\n");
                        compte.debit = Convert.ToInt32(Console.ReadLine());
                        if (compte.sold - compte.debit < 0.00)
                        {

                            Console.WriteLine("ERREUR : Impossible de débiter votre compte.\n");
                            Console.WriteLine("Fonds insuffisants, vous n'êtes pas autorisés à posséder un solde négatif (" + (compte.sold - compte.debit).ToString() +
                            " € en trop sur le débit total de " + (compte.sold - (compte.sold - compte.debit)).ToString() + "€)\n");
                            while (notInErrorFormat)
                            {
                                Console.WriteLine("Voulez-vous rententer de débiter à nouveau ?\n");
                                Console.WriteLine("1 - Oui\n");
                                Console.WriteLine("2 - Non\n");
                                yesno = Convert.ToInt32(Console.ReadLine());
                                if (yesno == 1)
                                {
                                    // Oui
                                    notInErrorFormat = false;
                                }
                                else if (yesno == 2)
                                {
                                    notInErrorFormat = false;
                                    keepTrying = false;
                                }
                                else
                                {
                                    Console.WriteLine("ERREUR : Format invalide.\n");
                                }
                            }
                            


                        }
                        else
                        {
                            compte.sold -= compte.debit;
                            Console.WriteLine(compte.debit + " € débités sur votre compte.\n");
                            Console.WriteLine("Votre solde possède dorénavant : " + compte.sold + " €\n");
                            keepTrying = false;
                        }
                    } 
                }
                else if (choix == 3)
                {
                    Console.WriteLine("Combien d'argent voulez-vous créditer sur votre compte, qui possède " + compte.sold.ToString() + " € ?\n");
                    compte.credit = Convert.ToInt32(Console.ReadLine());
                    compte.sold += compte.credit;
                    Console.WriteLine(compte.credit + " € crédités sur votre compte.\n");
                    Console.WriteLine("Votre solde possède dorénavant : " + compte.sold + " €\n");
                }
                else if (choix == 4)
                {
                    Console.WriteLine("Vous quittez votre espace banquaire. Nous vous souhaitons une bonne journée !\n");
                }
            }
        }
    }
}
